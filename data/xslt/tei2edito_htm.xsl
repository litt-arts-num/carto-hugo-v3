<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating HTML editorial pages
* version 2023-06-28
* @author AnneGF@CNRS
* @date : 2022-2023
**/
-->
<!DOCTYPE tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:strip-space elements="*"/>
    <xsl:preserve-space elements="tei:name tei:ab tei:bibl tei:note tei:title tei:respStmt"/>

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:TEI">
        <xsl:result-document href="../auto/htm/about.htm" method="xhtml" indent="yes">
            <xsl:apply-templates select="tei:teiHeader/tei:encodingDesc/tei:projectDesc"/>
            <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:titleStmt" mode="team"/>
            <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:publicationStmt"/>
            <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:sourceDesc"/>
        </xsl:result-document>
        <xsl:result-document href="../auto/htm/edition.htm" method="xhtml" indent="yes">
            <xsl:apply-templates select="tei:teiHeader/tei:encodingDesc/tei:editorialDecl"/>
        </xsl:result-document>
        <!--
                    <style>
            ul.dashed-list {
                list-style-type: none;
            }
            ul.dashed-list li::before {
                content: '\2014';
                position: absolute;
                margin-left: -20px;
            }</style>
        <xsl:apply-templates/>
        -->
    </xsl:template>

    <!-- Template autofermant qui permet, pour un élément donné, de ne rien faire -->
    <xsl:template match="tei:text"/>
    <xsl:template match="tei:facsimile"/>
    <xsl:template match="tei:teiHeader">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:profileDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:fileDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:titleStmt" mode="team">
        <h2 id="team">L'équipe</h2>
        <ul>
            <li>Responsable du projet : <xsl:apply-templates select="tei:principal" mode="titleStmt"
                /></li>
            <xsl:apply-templates select="tei:respStmt"/>
        </ul>
        <p id="logos">
            <xsl:for-each select="tei:funder | tei:sponsor">
                <img class="logo" src="assets/img/logos/{@facs}" title="{normalize-space(.)}"/>
            </xsl:for-each>
        </p>
    </xsl:template>
    <xsl:template match="tei:title" mode="titleStmt">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:author" mode="titleStmt">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:principal" mode="titleStmt">
        <xsl:apply-templates mode="titleStmt"/>
    </xsl:template>
    <xsl:template match="tei:respStmt">
        <li>
            <xsl:apply-templates select="tei:resp" mode="titleStmt"/>
            <xsl:text> : </xsl:text>
            <xsl:for-each select="tei:name">
                <xsl:if test="position() > 1">
                    <xsl:choose>
                        <xsl:when test="position() = last()">
                            <xsl:text> et </xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>, </xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
                <xsl:value-of select="normalize-space(.)"/>
                <xsl:if test="position() = last()">
                    <xsl:text>.</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </li>
    </xsl:template>

    <xsl:template match="tei:langUsage"/>
    <xsl:template match="tei:langUsage" mode="dont_use_it">
        <h2 id="lang">Langues</h2> Les langues présentes dans cette source sont :
            <ul><xsl:apply-templates/></ul>
    </xsl:template>
    <xsl:template match="tei:language">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:encodingDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:projectDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:publicationStmt">
        <h2 id="website">
            <xsl:text> Site </xsl:text>
            <a target="_blank" href="{tei:ptr[@type='source_code']/@target}"
                class="btn btn-light btn-sm" role="button"
                style="margin: 4px; float: right; font-size: small;">Code source</a>
        </h2>
        <xsl:apply-templates/>
        <xsl:apply-templates select="ancestor::tei:teiHeader/tei:encodingDesc/tei:appInfo"/>
    </xsl:template>
    <xsl:template match="tei:authority">
        <p>Ce site est publié par l'<xsl:apply-templates/>.</p>
    </xsl:template>
    <xsl:template match="tei:availability">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:ptr[@type = 'source_code']"/>

    <xsl:template match="tei:appInfo">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:application">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:desc">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="tei:sourceDesc">
        <h2 id="sources">Sources</h2>
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:bibl">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:author">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:listPlace"/>
    <xsl:template match="tei:editorialDecl">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:revisionDesc">
        <h2>Notes de version</h2>
        <xsl:apply-templates/>
    </xsl:template>

    <!-- Template qui permet pour chaque élément tei:p, de générer un élément HTML <p/>, 
        puis, concernant le contenu, d'appliquer les autres templates -->
    <xsl:template match="tei:p">
        <xsl:choose>
            <xsl:when
                test="count(child::text()/normalize-space()[not(. = '')]) = 0 and count(child::tei:hi) = 1">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:when
                test="count(child::text()/normalize-space()[not(. = '')]) = 0 and count(child::tei:list) = 1">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:apply-templates/>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:lb">
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'bold']">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'title']">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="tei:list[@rend = 'dash']">
        <ul class="dashed-list">
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:list[@type = 'bulleted']">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template match="tei:title[@level = 'm']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:placeName">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:date">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="replace(normalize-space(.), ' :', '&nbsp;:')"/>
    </xsl:template>

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
