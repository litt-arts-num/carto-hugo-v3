<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating roman.htm
* version 2023-06-28
* @author AnneGF@CNRS
* @date : 2022-2023
**/
--><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">
    <xsl:output method="html" indent="yes" encoding="UTF-8" version="draft"/>
    <!--
        This XSL use a XML-TEI input file and generate an HTML edition of the source.
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
        
        TODO? Check if @style has an correct format (css style)
    -->

    <xsl:variable name="DEBUG">0</xsl:variable>

    <xsl:template match="/tei:TEI">
        <div id="roman" class="card-body">
            <div style="font-size: small">
                <ul id="menu">
                    <xsl:for-each select="tei:text/tei:body/tei:div/tei:head">
                        <xsl:variable name="title_id">
                            <xsl:number count="tei:head" level="any"/>
                        </xsl:variable>
                        <li id="h{$title_id}">
                            <xsl:if test="@type">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="@type"/>
                                </xsl:attribute>
                                <xsl:if
                                    test="not(contains(' orig ajout ', concat(' ', @type, ' ')))">
                                    <xsl:variable name="msg">
                                        <xsl:text>Valeur de @type "</xsl:text>
                                        <xsl:value-of select="@type"/>
                                        <xsl:text>" pour un élément &lt;head/> non attendue.</xsl:text>
                                    </xsl:variable>
                                    <xsl:if test="$DEBUG = 1">
                                        <xsl:message>
                                            <xsl:value-of select="$msg"/>
                                        </xsl:message>
                                    </xsl:if>
                                </xsl:if>
                            </xsl:if>
                            <a href="#h{$title_id}">
                                <xsl:apply-templates/>
                            </a>
                            <xsl:if test="count(following-sibling::tei:*/tei:head) > 0">
                                <ul>
                                    <xsl:for-each select="following-sibling::tei:*/tei:head">
                                        <xsl:variable name="subtitle_id">
                                            <xsl:number count="tei:head" level="any"/>
                                        </xsl:variable>
                                        <li id="call_h{$subtitle_id}">
                                            <xsl:if test="@type">
                                                <xsl:attribute name="class">
                                                  <xsl:value-of select="@type"/>
                                                </xsl:attribute>
                                                <xsl:if
                                                  test="not(contains(' orig ajout ', concat(' ', @type, ' ')))">
                                                  <xsl:variable name="msg">
                                                  <xsl:text>Valeur de @type "</xsl:text>
                                                  <xsl:value-of select="@type"/>
                                                  <xsl:text>" pour un élément &lt;head/> non attendue.</xsl:text>
                                                  </xsl:variable>
                                                    <xsl:if test="$DEBUG = 1">
                                                        <xsl:message>
                                                            <xsl:value-of select="$msg"/>
                                                        </xsl:message>
                                                    </xsl:if>
                                                </xsl:if>
                                            </xsl:if>
                                            <a href="#h{$subtitle_id}">
                                                <xsl:apply-templates/>
                                            </a>
                                            <xsl:if
                                                test="count(following-sibling::tei:*/tei:head) > 0">
                                                <ul>
                                                  <xsl:for-each
                                                  select="following-sibling::tei:*/tei:head">
                                                  <xsl:variable name="subsubtitle_id">
                                                  <xsl:number count="tei:head" level="any"/>
                                                  </xsl:variable>
                                                  <li id="call_h{$subsubtitle_id}">
                                                  <xsl:if test="@type">
                                                  <xsl:attribute name="class">
                                                  <xsl:value-of select="@type"/>
                                                  </xsl:attribute>
                                                  <xsl:if
                                                  test="not(contains(' orig ajout ', concat(' ', @type, ' ')))">
                                                  <xsl:variable name="msg">
                                                  <xsl:text>Valeur de @type "</xsl:text>
                                                  <xsl:value-of select="@type"/>
                                                  <xsl:text>" pour un élément &lt;head/> non attendue.</xsl:text>
                                                  </xsl:variable>
                                                  <xsl:if test="$DEBUG = 1">
                                                  <xsl:message>
                                                  <xsl:value-of select="$msg"/>
                                                  </xsl:message>
                                                  </xsl:if>
                                                  </xsl:if>
                                                  </xsl:if>
                                                  <a href="#h{$subsubtitle_id}">
                                                  <xsl:apply-templates/>
                                                  </a>
                                                  </li>
                                                  </xsl:for-each>
                                                </ul>
                                            </xsl:if>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </xsl:if>
                        </li>

                    </xsl:for-each>
                </ul>
            </div>
            <xsl:apply-templates select="tei:text"/>
        </div>
        <hr/>
        <xsl:apply-templates select="//tei:note[@type = 'aut']" mode="generate-footnote"/>
        <div class="modal fade" id="noteModale" tabindex="-1" role="dialog" style="display: none;"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="noteModaleLabel">Note de l'éditeur</h5>
                    </div>
                    <div class="modal-body text-justify"> </div>
                </div>
            </div>
        </div>
        <xsl:apply-templates select="//tei:note[@type = 'ed']" mode="generate-modal"/>
    </xsl:template>

    <xsl:template match="tei:text">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:front">
        <div class="front">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:body">
        <div class="body">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:div">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@type @subtype</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <div class="{@type} {@subtype}">
            <xsl:if test="@type">
                <xsl:if test="not(contains(' chap preliminary_chap oeuvre part livre ', concat(' ', @type, ' ')))">
                    <xsl:variable name="msg">
                        <xsl:text>Valeur de @type "</xsl:text>
                        <xsl:value-of select="@type"/>
                        <xsl:text>" pour un élément &lt;div/> non attendue.</xsl:text>
                    </xsl:variable>
                    <xsl:if test="$DEBUG = 1">
                        <xsl:message>
                            <xsl:value-of select="$msg"/>
                        </xsl:message>
                    </xsl:if>
                </xsl:if>
            </xsl:if>
            <xsl:if test="@subtype">
                <xsl:if test="not(contains(' orig ajout ', concat(' ', @subtype, ' ')))">
                    <xsl:variable name="msg">
                        <xsl:text>Valeur de @subtype "</xsl:text>
                        <xsl:value-of select="@subtype"/>
                        <xsl:text>" pour un élément &lt;subtype/> non attendue.</xsl:text>
                    </xsl:variable>
                    <xsl:if test="$DEBUG = 1">
                        <xsl:message>
                            <xsl:value-of select="$msg"/>
                        </xsl:message>
                    </xsl:if>
                </xsl:if>
            </xsl:if>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:head">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@n @type</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:if test="@type">
            <xsl:if test="not(contains(' orig ajout ', concat(' ', @type, ' ')))">
                <xsl:variable name="msg">
                    <xsl:text>Valeur de @type "</xsl:text>
                    <xsl:value-of select="@type"/>
                    <xsl:text>" pour un élément &lt;div/> non attendue.</xsl:text>
                </xsl:variable>
                <xsl:if test="$DEBUG = 1">
                    <xsl:message>
                        <xsl:value-of select="$msg"/>
                    </xsl:message>
                </xsl:if>
            </xsl:if>
        </xsl:if>
        <xsl:variable name="title_level">
            <xsl:value-of select="count(ancestor::*[count(ancestor::tei:body) > 0]) + 1"/>
        </xsl:variable>
        <xsl:variable name="title_id">
            <xsl:number count="tei:head" level="any"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@type = 'ajout'">
                <div class="add bg-secondary text-light" title="Texte ajouté">
                    <xsl:element name="h{$title_level}">
                        <xsl:attribute name="id">
                            <xsl:text>h</xsl:text>
                            <xsl:value-of select="$title_id"/>
                        </xsl:attribute>
                        <span style="font-size: small; vertical-align: top;">
                            <i class="fas fa-plus-circle"/>
                        </span>
                        <xsl:apply-templates/>
                        <xsl:if test="4 > $title_level">
                            <span class="m-2">
                                <a href="#call_h{$title_id}">
                                    <i class="fas fa-arrow-circle-up"/>
                                </a>
                            </span>
                        </xsl:if>
                    </xsl:element>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="h{$title_level}">
                    <xsl:attribute name="id">
                        <xsl:text>h</xsl:text>
                        <xsl:value-of select="$title_id"/>
                    </xsl:attribute>
                    <xsl:if test="@type = 'orig'">
                        <xsl:attribute name="class">
                            <xsl:text>orig</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates/>
                    <xsl:if test="4 > $title_level">
                        <span class="m-2">
                            <a href="#call_h{$title_id}">
                                <i class="fas fa-arrow-circle-up"/>
                            </a>
                        </span>
                    </xsl:if>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:note[@type = 'ed']" mode="generate-modal">
        <xsl:variable name="id-note">
            <xsl:number count="tei:note[@type = 'ed']" level="any"/>
        </xsl:variable>
        <div style="display:none;" data-note-content-id="{$id-note}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:note[@type = 'aut']" mode="generate-footnote">
        <xsl:variable name="id-note">
            <xsl:number count="tei:note[@type = 'aut']" level="any"/>
        </xsl:variable>
        <div id="n{$id-note}">
            <a href="#call_n{$id-note}">
                <xsl:value-of select="$id-note"/>
                <xsl:text>. </xsl:text>
            </a>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:note">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@type</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:choose>
            <xsl:when test="@type = 'ed'">
                <xsl:variable name="id-note">
                    <xsl:number count="tei:note[@type = 'ed']" level="any"/>
                </xsl:variable>
                <sup class="note-call" aria-hidden="true" data-note-id="{$id-note}">
                    <i class="fas fa-info-circle"/>
                </sup>
            </xsl:when>
            <xsl:when test="@type = 'aut'">
                <xsl:variable name="id-note">
                    <xsl:number count="tei:note[@type = 'aut']" level="any"/>
                </xsl:variable>
                <sup id="call_n{$id-note}">
                    <a href="#n{$id-note}">
                        <xsl:value-of select="$id-note"/>
                    </a>
                </sup>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="msg">
                    <xsl:text>Balise "tei:note", type "</xsl:text>
                    <xsl:value-of select="@type"/>
                    <xsl:text>" non géré</xsl:text>
                </xsl:variable>
                <xsl:if test="$DEBUG = 1">
                    <xsl:message>
                        <xsl:value-of select="$msg"/>
                    </xsl:message>
                </xsl:if>
                <i class="fa fa-exclamation-triangle">
                    <xsl:attribute name="title">
                        <xsl:value-of select="$msg"/>
                    </xsl:attribute>
                </i>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:p">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@xml:lang</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <p>
            <xsl:if test="@xml:lang">
                <sup>
                    <i class="fa fa-info">
                        <xsl:attribute name="title">
                            <xsl:text>Texte en </xsl:text>
                            <xsl:choose>
                                <xsl:when
                                    test="//tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]">
                                    <xsl:value-of
                                        select="//tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]"
                                    />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:variable name="msg">
                                        <xsl:text>Valeur @xml:lang "</xsl:text>
                                        <xsl:value-of select="@xml:lang"/>
                                        <xsl:text>" non déclarée</xsl:text>
                                    </xsl:variable>
                                    <xsl:if test="$DEBUG = 1">
                                        <xsl:message>
                                            <xsl:value-of select="$msg"/>
                                        </xsl:message>
                                    </xsl:if>
                                    <xsl:value-of select="@xml:lang"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                    </i>
                </sup>
            </xsl:if>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="tei:lg">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text/>
            </xsl:with-param>
        </xsl:call-template>
        <p class="verse mb-1 ml-2">
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="tei:l">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text/>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates/>
        <br/>
    </xsl:template>

    <xsl:template match="tei:lb">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text/>
            </xsl:with-param>
        </xsl:call-template>
        <br/>
    </xsl:template>

    <xsl:template match="tei:hi">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@rend</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:choose>
            <xsl:when test="@rend = 'italic'">
                <i>
                    <xsl:apply-templates/>
                </i>
            </xsl:when>
            <xsl:when test="@rend = 'exponent'">
                <sup>
                    <xsl:apply-templates/>
                </sup>
            </xsl:when>
            <xsl:otherwise>
                <span>
                    <xsl:variable name="msg">
                        <xsl:text>Balise "tei:hi", valeur de l'attribut @rend "</xsl:text>
                        <xsl:value-of select="@rend"/>
                        <xsl:text>" non gérée</xsl:text>
                    </xsl:variable>
                    <xsl:if test="$DEBUG = 1">
                        <xsl:message>
                            <xsl:value-of select="$msg"/>
                        </xsl:message>
                    </xsl:if>
                    <i class="fa fa-exclamation-triangle">
                        <xsl:attribute name="title">
                            <xsl:value-of select="$msg"/>
                        </xsl:attribute>
                    </i>
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:seg[@type = 'ajout']">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@type</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <span class="add m-1 p-1 bg-secondary text-light" title="Texte ajouté">
            <i class="fas fa-plus-circle"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:epigraph">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@style</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <div style="{@style}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:ref">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@style</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <span style="{@style}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:placeName">
        <xsl:call-template name="check-attribute">
            <xsl:with-param name="known_attributes">
                <xsl:text>@ref</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:if test="@ref">
            <xsl:if test="not(//tei:place[concat('#', @xml:id) = current()/@ref])">
                <xsl:variable name="msg">
                    <xsl:text>Valeur de @ref "</xsl:text>
                    <xsl:value-of select="@ref"/>
                    <xsl:text>" pour un élément &lt;placeName/> ne correspondant pas aucun attribut @xml:id d'élément &lt;place/>.</xsl:text>
                </xsl:variable>
                <xsl:if test="$DEBUG = 1">

                        <xsl:value-of select="$msg"/>
                    
                </xsl:if>
            </xsl:if>
        </xsl:if>
        <span class="location" data-location="{substring-after(@ref, '#')}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:*">
        <xsl:variable name="msg">
            <xsl:text>Balise "tei:</xsl:text>
            <xsl:value-of select="name(.)"/>
            <xsl:text>" non gérée</xsl:text>
        </xsl:variable>
        <xsl:if test="$DEBUG = 1">
            <xsl:message>
                <xsl:value-of select="$msg"/>
            </xsl:message>
        </xsl:if>
        <i class="fa fa-exclamation-triangle">
            <xsl:attribute name="title">
                <xsl:value-of select="$msg"/>
            </xsl:attribute>
        </i>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template name="check-attribute">
        <xsl:param name="known_attributes"/>
        <xsl:for-each select="@*">
            <xsl:if test="not(contains(concat($known_attributes, ' '), concat('@', name(.), ' ')))">
                <xsl:variable name="msg">
                    <xsl:text>Attribut "</xsl:text>
                    <xsl:value-of select="name(.)"/>
                    <xsl:text>" de la balise "</xsl:text>
                    <xsl:value-of select="name(./parent::node())"/>
                    <xsl:text>" non géré.</xsl:text>
                </xsl:variable>
                <xsl:if test="$DEBUG = 1">
                    <xsl:message>
                        <xsl:value-of select="$msg"/>
                    </xsl:message>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
