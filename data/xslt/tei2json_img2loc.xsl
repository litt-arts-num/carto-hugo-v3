<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating imgToLocations.json
* version 2023-06-28
* @author AnneGF@CNRS
* @date : 2022-2023
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!--
        This XSL use a XML-TEI input file and generate a json file containing a list of locations (with zone information) organized by image based on <tei:surface/>)
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
        Output example:
        ```
        {
            "CG_1781.jpg": {
                "Ancresse": [
                    ["2627", "4323", "108", "47"]
                ],
                (...)
            },
            (...)
        }
    -->
    <xsl:variable name="DEBUG">0</xsl:variable>

    <xsl:variable name="br">
        <xsl:text>
</xsl:text>
    </xsl:variable>

    <xsl:template match="/tei:TEI">
        <xsl:text>{</xsl:text>
        <xsl:value-of select="$br"/>
        <xsl:for-each select="tei:facsimile/tei:surface">
            <xsl:text>  "</xsl:text>
            <xsl:value-of select="@xml:id"/>
            <xsl:text>": {</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:for-each-group select="tei:zone" group-by="@corresp">
                <xsl:sort select="current-grouping-key()" lang="fr"/>
                <xsl:text>    "</xsl:text>
                <xsl:value-of select="replace(current-grouping-key(), '#', '')"/>
                <xsl:text>": [</xsl:text>
                <xsl:value-of select="$br"/>
                <xsl:for-each select="current-group()">
                    <xsl:text>      ["</xsl:text>
                    <xsl:value-of select="@ulx"/>
                    <xsl:text>", "</xsl:text>
                    <xsl:value-of select="@uly"/>
                    <xsl:text>", "</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@lrx and not(@lrx = '')">
                            <xsl:value-of select="xs:integer(@lrx) - xs:integer(@ulx)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>NaN</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:text>", "</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@lry and not(@lry = '')">
                            <xsl:value-of select="xs:integer(@lry) - xs:integer(@uly)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>NaN</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>"]</xsl:text>
                    <xsl:if test="not(position() = last())">
                        <xsl:text>,</xsl:text>
                        <xsl:value-of select="$br"/>
                    </xsl:if>
                </xsl:for-each>
                <xsl:value-of select="$br"/>
                <xsl:text>    ]</xsl:text>
                <xsl:if test="not(position() = last())">
                    <xsl:text>,</xsl:text>
                    <xsl:value-of select="$br"/>
                </xsl:if>
            </xsl:for-each-group>
            <xsl:value-of select="$br"/>
            <xsl:text>  }</xsl:text>
            <xsl:if test="not(position() = last())">
                <xsl:text>,</xsl:text>
            </xsl:if>
            <xsl:value-of select="$br"/>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
        <xsl:value-of select="$br"/>
    </xsl:template>
</xsl:stylesheet>
