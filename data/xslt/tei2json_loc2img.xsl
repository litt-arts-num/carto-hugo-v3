<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating locationsToImg.json
* version 2023-06-28
* @author AnneGF@CNRS
* @date : 2022-2023
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!--
        This XSL use a XML-TEI input file and generate a json file containing a list of locations (with corresponding images info) based on <tei:surface/>)
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
        Output example:
        ```
        {
          "Ancresse": [
            "CG_1781.jpg",
            "CG_1852.jpg",
            "CG_1871.jpg",
            "NAF_13459_9v.jpg",
            "NAF_13460-82v.jpg"
          ],
          (...)
        }
    -->
    <xsl:variable name="DEBUG">0</xsl:variable>

    <xsl:variable name="br">
        <xsl:text>
</xsl:text>
    </xsl:variable>

    <xsl:template match="/tei:TEI">
        <xsl:text>{</xsl:text>
        <xsl:value-of select="$br"/>
        <xsl:for-each-group select="tei:facsimile/tei:surface/tei:zone" group-by="@corresp">
            <xsl:sort select="@corresp" lang="fr"></xsl:sort>
            <xsl:text>  "</xsl:text>
            <xsl:value-of select="replace(current-grouping-key(), '#', '')"/>
            <xsl:text>": [</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:for-each select="current-group()">
                <xsl:text>    "</xsl:text>
                <xsl:value-of select="parent::tei:surface/@xml:id"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="not(position() = last())">
                    <xsl:text>,</xsl:text>
                    <xsl:value-of select="$br"/>
                </xsl:if>
            </xsl:for-each>
            <xsl:value-of select="$br"/>
            <xsl:text>  ]</xsl:text>
            <xsl:if test="not(position() = last())">
                <xsl:text>,</xsl:text>
                <xsl:value-of select="$br"/>
            </xsl:if>
        </xsl:for-each-group>
        <xsl:value-of select="$br"/>
        <xsl:text>}</xsl:text>
        <xsl:value-of select="$br"/>
    </xsl:template>
</xsl:stylesheet>
