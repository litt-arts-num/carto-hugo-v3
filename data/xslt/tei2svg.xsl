<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating svg images
* version 2023-06-28
* @author AnneGF@CNRS
* @date : 2022-2023
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <xsl:output method="text" indent="yes" encoding="UTF-8" version="1.0"/>

    <xsl:variable name="DEBUG">0</xsl:variable>

    <xsl:template match="/tei:TEI">
        <xsl:for-each select="tei:facsimile/tei:surface">
            <xsl:variable name="file">
                <xsl:text>/</xsl:text>
                <xsl:value-of select="string-join(tokenize(base-uri(), '/')[position()>1 and (last()-1)>position()], '/')"/>
                <xsl:text>/auto/svg/</xsl:text>
                <xsl:value-of select="@xml:id"/>
                <xsl:text>.svg</xsl:text>
            </xsl:variable>
            <xsl:if test="$DEBUG = 1"><xsl:message>
                <xsl:value-of select="$file"/>
            </xsl:message></xsl:if>
            <xsl:variable name="width" select="substring-before(tei:graphic/@width, 'px')"/>
            <xsl:variable name="height" select="substring-before(tei:graphic/@height, 'px')"/>
            <xsl:result-document href="{$file}" method="xml" version="1.0" encoding="UTF-8"
                indent="yes">
                <xsl:text disable-output-escaping="yes">
&lt;?xml-stylesheet href='../../../assets/css/svg.css'?>
</xsl:text>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <xsl:attribute name="width" select="$width"/>
                    <xsl:attribute name="height" select="$height"/>
                    <xsl:attribute name="viewBox">
                        <xsl:text>0 0 </xsl:text>
                        <xsl:value-of select="$width"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="$height"/>
                    </xsl:attribute>
                    <image xlink:href="../../thumbnails/{substring-after(tei:graphic/@url,'/img/')}" width="{$width}" height="{$height}"/>
                    <xsl:for-each select="tei:zone">
                        <xsl:variable name="location" select="substring-after(@corresp, '#')"/>
                        <xsl:variable name="zone_width" select="@lrx - @ulx"/>
                        <xsl:variable name="zone_height" select="@lry - @uly"/>
                        <g class="location location-hidden" data-location="{$location}">
                            <title>
                                <xsl:value-of
                                    select="//tei:place[@xml:id = $location]/tei:placeName"/>
                            </title>
                            <rect width="{$zone_width}" height="{$zone_height}" x="{@ulx}"
                                y="{@uly}"/>
                        </g>
                    </xsl:for-each>
                </svg>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
