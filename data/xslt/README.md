# Files and folders organisation
```bash
├── data
│   ├── auto
│   │   ├── htm
│   │   │   ├── about.htm
│   │   │   ├── edition.htm
│   │   │   └── roman.htm
│   │   ├── js
│   │   │   ├── imgToLocations.json
│   │   │   ├── locations.json
│   │   │   └── locationsToImg.json
│   │   └── svg
```

# XSLT files
* tei2html.xsl : Generate the web editions of the source (`<div id="roman" class="card-body"/>`)
* tei2edito_htm.xsl : Generate 2 htm files: about.htm and edition.htm
* tei2svg.xsl : Generate svg files
* tei2json* : _generate json files_
    * tei2json_img2loc.xsl : Generate imgToLocations.json
    * tei2json_loc.xsl : Generate locations.json
    * tei2json_loc2img.xsl : Generate locationsToImg.json  

# Useful command line
```bash
apt install default-jre libsaxon-java

# from the data/xslt/ directory
cd data/xslt
# generate htm files
xsltproc -o ../auto/htm/roman.htm ./tei2html.xsl ../tei/cartohugo_travailleursdelamer.xml
saxonb-xslt  -ext:on ../tei/cartohugo_travailleursdelamer.xml ./tei2edito_htm.xsl
# generate json files
saxonb-xslt  -ext:on ../tei/cartohugo_travailleursdelamer.xml ./tei2json_img2loc.xsl > ../auto/js/imgToLocations.json
saxonb-xslt  -ext:on ../tei/cartohugo_travailleursdelamer.xml ./tei2json_loc.xsl > ../auto/js/locations.json
saxonb-xslt  -ext:on ../tei/cartohugo_travailleursdelamer.xml ./tei2json_loc2img.xsl > ../auto/js/locationsToImg.json
# and to generate svg files:
saxonb-xslt  -ext:on ../tei/cartohugo_travailleursdelamer.xml ./tei2svg.xsl > ../auto/svg/tei2svg.log
```
