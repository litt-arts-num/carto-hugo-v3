<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating locations.json
* version 2023-06-28
* @author AnneGF@CNRS
* @date : 2022-2023
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!--
        This XSL use a XML-TEI input file and generate a json file containing a list of locations (with names) based on <tei:surface/>)
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
        Output example:
        ```
        {
          "Trois_Pierres": {
            "locations": "Trois_Pierres",
            "value": "Pierres (Trois)"
        },
        (...)
        }
    -->
    <xsl:variable name="DEBUG">0</xsl:variable>

    <xsl:variable name="br">
        <xsl:text>
</xsl:text>
    </xsl:variable>

    <xsl:template name="generate_json_place">
        <xsl:param name="place" required="yes"/>
        <xsl:param name="parent_string" required="yes"/>

        <xsl:text>  "</xsl:text>
        <xsl:value-of select="@xml:id"/>
        <xsl:text>": {</xsl:text>
        <xsl:value-of select="$br"/>
        <xsl:text>    "locations": "</xsl:text>
        <xsl:value-of select="@xml:id"/>
        <xsl:if test="$place//tei:place">
            <xsl:for-each select="$place//tei:place">
                <xsl:text> </xsl:text>
                <xsl:value-of select="@xml:id"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:text>",</xsl:text>
        <xsl:value-of select="$br"/>
        <xsl:text>    "value": "</xsl:text>
        <xsl:value-of select="$parent_string" disable-output-escaping="yes"/>
        <xsl:choose>
            <xsl:when test="tei:note">
                <xsl:value-of select="tei:note"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="tei:placeName">
                    <xsl:sort select="." lang="fr"/>
                    <xsl:value-of select="."/>
                    <xsl:if test="not(position() = last())">
                        <xsl:text> ou </xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>"</xsl:text>
        <xsl:value-of select="$br"/>
        <xsl:text>  }</xsl:text>
        <xsl:if test="tei:place">
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:variable name="this_parent_string">
                <xsl:value-of select="$parent_string"/>
                <xsl:value-of select="tei:placeName"/>
                <xsl:text disable-output-escaping="yes"> > </xsl:text>
            </xsl:variable>
            <xsl:for-each select="tei:place">
                <xsl:sort select="." lang="fr"/>
                <xsl:call-template name="generate_json_place">
                    <xsl:with-param name="place" select="current()"/>
                    <xsl:with-param name="parent_string" select="$this_parent_string"/>
                </xsl:call-template>
                <xsl:if test="not(position() = last())">
                    <xsl:text>,</xsl:text>
                    <xsl:value-of select="$br"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template match="/tei:TEI">
        <xsl:text>{</xsl:text>
        <xsl:value-of select="$br"/>
        <xsl:for-each select=".//tei:listPlace/tei:place">
            <xsl:sort select="." lang="fr"/>
            <xsl:call-template name="generate_json_place">
                <xsl:with-param name="place" select="current()"/>
                <xsl:with-param name="parent_string">
                    <xsl:text/>
                </xsl:with-param>
            </xsl:call-template>
            <xsl:if test="not(position() = last())">
                <xsl:text>,</xsl:text>
                <xsl:value-of select="$br"/>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
        <xsl:value-of select="$br"/>
    </xsl:template>
</xsl:stylesheet>
