<h2>Présentation</h2>
<p>L’imagination de Victor Hugo est très largement visuelle : elle s’exprime bien sûr au moyen d’images poétiques et romanesques
   mais aussi à travers les célèbres dessins de l’écrivain. Cependant, on sait peut-être moins que cette imagination est aussi
   géométrique et topographique. L’appréhension de l’espace est chez Victor Hugo un puissant accélérateur de la créativité littéraire.
</p>
<p>L’objectif du projet « Cartographies hugoliennes » (Carto-Hugo) est d’étudier, à travers les manuscrits de l’écrivain, la
   façon dont la représentation spatiale et cartographique engendre l’univers littéraire et structure l’imagination romanesque.
   À titre exploratoire et expérimental, le travail se concentre sur un cas particulier, un corpus de brouillons et de dessins
   ayant conduit, à partir du premier carnet de prise de notes en 1859, à la création du roman d’aventures maritimes
   <i>Les Travailleurs de la mer</i>en 1866.
</p>
<p>Durant la conception de son roman, Hugo réalise des cartes que l’on pourra découvrir sur le site. Il s’attarde sur des lieux
   qui vont devenir cruciaux pour l’intrigue. En dessinant ses cartes, Hugo sélectionne des sites, construit des points de vue
   qu’il restitue ensuite par le dessin et par l’écriture dans d’autres feuillets. Il précise également des décors, inspirés
   de croquis réalisés sur le motif. Il peut aussi matérialiser les trajectoires de ses personnages dans l’espace.
</p>
<p>Actuellement, le prototype du site CartoHugo permet une première approche de ces mécanismes génétiques. L’interface présente
   deux entrées qui offrent des explorations différentes.
</p>
<ul>
   <li>Une entrée via le texte du roman, où sont balisées les références et qui permet de visualiser les données cartographiques
      ainsi que des données génétiques liées à l’espace : croquis, dessins, notes.
   </li>
   <li>Une seconde entrée est proposée via les cartes, les manuscrits de Victor Hugo et les données iconographiques. Les documents
      sont traités de manière à isoler des zones cliquables qui correspondent aux références topographiques dans le texte et qui
      permettent ainsi de voir pour un passage du roman donné les recherches effectuées par l’écrivain.
   </li>
</ul>
<p>L’objectif est d’apprécier les liens entre espace géographique et espace romanesque sans rapporter strictement l’un à l’autre
   mais pour mesurer au contraire le travail créatif de l’écrivain à l’œuvre dans l’espace génétique des manuscrits.
</p>
<h2 id="team">L'équipe</h2>
<ul>
   <li>Responsable du projet : Delphine
      						Gleizes, UGA,
      						Litt&amp;Arts
   </li>
   <li>Établissement du texte, balisage TEI et repérages cartographiques : Delphine Gleizes.</li>
   <li>Relecture du texte et balisage TEI : Loup Belliard.</li>
   <li>Supervision de l'encodage, formation : Elisabeth Greslou et Anne Garcia-Fernandez.</li>
   <li>Préparation initiale des données de lieu : Célia Marion.</li>
   <li>Structuration et squelette du site : Arnaud Bey.</li>
   <li>Visualisation des données : Arnaud Bey, Maxime Bouton, Anne Garcia-Fernandez et Vincent Maillard.</li>
</ul>
<p id="logos">
   <img class="logo" src="assets/img/logos/logo_littarts.png"
        title="Litt&amp;Arts (UMR 5316, UGA / CNRS)"></img>
   <img class="logo" src="assets/img/logos/logo_elan.png"
        title="ELAN, l'élan littératures, arts et numérique de Litt&amp;Arts (ELAN, Litt&amp;Arts)"></img>
   <img class="logo" src="assets/img/logos/logo_hn.jpg"
        title="laTGIR Huma-Num (hébergement du site web)"></img>
   <img class="logo" src="assets/img/logos/logo_demarre_shs.jpg"
        title="projet Démarre-SHS"></img>
   <img class="logo" src="assets/img/logos/logo_cnrs.png" title="CNRS"></img>
   <img class="logo" src="assets/img/logos/logo_univ_grenoble_alpes.jpg" title="UGA"></img>
</p>
<h2 id="website"> Site 
   <a target="_blank" href="https://gitlab.com/litt-arts-num/carto-hugo-v3"
      class="btn btn-light btn-sm"
      role="button"
      style="margin: 4px; float: right; font-size: small;">Code source</a>
</h2>
<p>Ce site est publié par l'UMR 5316 Litt&amp;Arts dans le cadre du projet Carto-Hugo.</p>
<p>Ce site et l'ensemble des textes qui y sont publiés sont proposés selon les termes de la
   <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons Attribution - Pas d'utilisation commerciale - Partage dans les mêmes conditions 4.0 International</a>.
</p>
<p>Les données du projet sont encodées en TEI P5 (dont la documentation est accessible à l'adresse
   <a href="https://tei-c.org/release/doc/tei-p5-doc/en/html/index.html">https://tei-c.org/release/doc/tei-p5-doc/en/html/index.html</a>) et accessibles
   <a href="/data/tei/">ici</a>. Ces données sont succeptibles d'évoluer (corrections, enrichissements...). Un dépôt dans un entrepôt spécialisé est prévu
   quand une version stable sera établie.
</p>
<p>Les sources TEI sont transformées en HTML, Javascript et SVG grâce à des feuilles de style XSLT (version 2.0 ou 1.0). Celles-ci
   sont accessibles
   <a href="/data/xslt/">ici</a>. Les sorties de ces fichiers sont disponibles
   <a href="/data/auto/">ici</a>.
</p>
<p>Les styles CSS du site web s'appuyent sur la collection d'outils Bootstrap dont la documentation est accessible sur
   <a href="https://getbootstrap.com/">https://getbootstrap.com/</a>.
</p>
<h2 id="sources">Sources</h2>
<ul>
   <li>
      <i>Les Travailleurs de la mer</i>, Paris, Lacroix et Verboeckhoven, 1866.Victor Hugo
   </li>
   <li>
      <i>L’Archipel de la Manche</i>, Paris, Calmann-Lévy, 1883.Victor Hugo
   </li>
   <li>Les clichés utilisés sur ce site sont pour la plupart issus de Gallica et des collections de la BnF. Les clichés notés MVH
      proviennent du site Paris Musées et des collections de la Maison de Victor Hugo. Les clichés reproduisant des livres illustrés
      viennent de collections particulières.
   </li>
</ul>