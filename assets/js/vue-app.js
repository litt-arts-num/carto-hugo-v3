const {
  createApp
} = Vue

var cartoApp = createApp({
  data() {
    return {
      openedImgsByLine: 6,
      imgsByLine: 2,
      locations: null,
      locationsToImg: null,
      imgToLocations: null,
      currentLocation: null,
      popups: {},
      pinnedLocations: [],
      searchLocation: "",
      searchPinnedLocation: "",
      onlyCurrentLocation: true,
      issue: '',
      test: 0,
      loading: true,
      rotationElements: [],
      rotationIndex: null
    }
  },
  methods: {
    filenameWithoutExt(filename){
      return filename.replace(/\.[^/.]+$/, "")
    },
    openImg(filename) {
      let currentImg = "data/images/" + filename

      if (!(filename in this.popups) || this.popups[filename].closed) {
        this.popups[filename] = window.open('img.html', filename, "menubar=1,resizable=1,width=800,height=800");
        this.popups[filename].addEventListener('load', function() {
          cartoApp.popups[filename].processParentMessage({
            'url': currentImg,
            'currentLocation': cartoApp.currentLocation
          });
        }, true);
      } else {
        this.popups[filename].focus()
      }

    },
    getLocationName(location) {
      return this.locations[location]["value"]
    },

    getSortedLocations() {
      const unsortedObjArr = [...Object.entries(this.locationsToImg)]
      const sortedObjArr = unsortedObjArr.sort(([key1, value1], [key2, value2]) =>
        key1.localeCompare(key2)
      )
      const sortedObject = {}
      sortedObjArr.forEach(([key, value]) => (sortedObject[key] = value))

      return sortedObject
    },
    countLocation(location) {
      let els
      els = document.querySelectorAll('#roman *[data-location="' + location + '"]')

      return els.length;
    },
    isLocationPresent(img) {
      if (!this.currentLocation) {
        return true
      } else {
        if (this.imgToLocations[img].hasOwnProperty(this.currentLocation)) {
          return true
        }
        return false
      }
    },
    makeClickable(evt) {
      let svg = evt.currentTarget
      let filename = svg.dataset.filename
      svg = svg.contentDocument;

      var els = svg.querySelectorAll("g");
      [].forEach.call(els, function(el) {
        el.addEventListener('click', function() {
          cartoApp.changeLocation(el.dataset.location)
        });
      });

    },
    popupExists(filename) {
      if (this.popups[filename].closed || !(filename in this.popups)) {
        return false
      }

      return true
    },

    unpin(location) {
      this.pinnedLocations.splice(this.pinnedLocations.indexOf(location), 1);
    },

    closeAll() {
      for (var popup in this.popups) {
        if (this.popups.hasOwnProperty(popup)) {
          this.popups[popup].close()
        }
      }
    },

    changeLocation(location) {
      this.currentLocation = location

      // voir dans quelle mesure c'est pratique... rendre ça optionnel ?
      // document.querySelector('#top-locations').scrollIntoView({
      //   behavior: "smooth",
      //   block: "end",
      //   inline: "nearest"
      // })

      // remove class
      let spans = document.querySelectorAll('#roman .match-location');
      [].forEach.call(spans, function(span) {
        span.classList.remove('match-location')
      });

      this.rotationElements = []
      // add class
      if (location != "") {
        this.rotationElements = document.querySelectorAll('#roman [data-location=' + location + ']');
        [].forEach.call(this.rotationElements, function(span) {
          span.classList.add('match-location')
        });
      }

      this.rotationIndex = null

    },
    rotate(forward) {
      if (this.rotationIndex == null) {
        this.rotationIndex = 0
      } else {
        let maxIndex = this.rotationElements.length - 1
        let increment = (forward) ? 1 : -1
        if ((forward && this.rotationIndex + 1 > maxIndex) || (!forward && this.rotationIndex - 1 < 0)) {
          this.rotationIndex = (forward) ? 0 : maxIndex
        } else {
          this.rotationIndex += increment
        }
      }

      this.rotationElements[this.rotationIndex].scrollIntoView({
        behavior: "smooth",
        block: "end",
        inline: "nearest"
      });
    }
  },
  mounted() {
    fetch("data/auto/htm/roman.htm")
      .then(res => res.text())
      .then(function(data) {
        document.querySelector("#roman").innerHTML = data

        let locations = document.querySelectorAll('[data-location]');
        for (var i = 0; i < locations.length; i++) {
          let location = locations[i].dataset.location
          locations[i].addEventListener('click', function() {
            cartoApp.changeLocation(location)
          });
        }

        cartoApp.loading = false
      });

    fetch("data/auto/js/locations.json")
      .then(res => res.json())
      .then(function(data) {
        cartoApp.locations = data
      });

    fetch("data/auto/js/locationsToImg.json")
      .then(res => res.json())
      .then(function(data) {
        cartoApp.locationsToImg = data
        cartoApp.test++
        // for (var key in data) {
        //   if (data.hasOwnProperty(key)) {
        //     console.log(key + " -> " + data[key]);
        //   }
        // }
      });

    fetch("data/auto/js/imgToLocations.json")
      .then(res => res.json())
      .then(data => cartoApp.imgToLocations = data);


    fetch("data/auto/htm/about.htm")
      .then(res => res.text())
      .then(function(data) {
        document.querySelector("#offcanvasAbout .offcanvas-body").innerHTML = data
      });

    fetch("data/htm/legal-notice.htm")
      .then(res => res.text())
      .then(function(data) {
        document.querySelector("#offcanvasLegal .offcanvas-body").innerHTML = data
      });

    fetch("data/auto/htm/edition.htm")
      .then(res => res.text())
      .then(function(data) {
        document.querySelector("#offcanvasEdition .offcanvas-body").innerHTML = data
      })

    // let svgs = document.querySelectorAll('object');
    // for (var i = 0; i < svgs.length; i++) {
    //   let svg = svgs[i].contentDocument
    //   svg.addEventListener('click', function() {
    //     // cartoApp.openImg()
    //   });
    // }
  }
}).mount('#app')


function ProcessChildMessage(message) {
  for (const type in message) {
    if (type == "choose-location") {
      cartoApp.changeLocation(message[type])
    }
    if (type == "close") {
      delete cartoApp.popups[message[type]]
    }
  }
}
