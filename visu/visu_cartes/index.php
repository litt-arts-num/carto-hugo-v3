<!DOCTYPE html>
<html lang="fr">

<head>

    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link rel="stylesheet" href="../../assets/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
    
    <script src="assets/js/data.js" defer></script>
    <script src="assets/js/tabs_events.js" defer></script>
    <script src="assets/js/javascript.js" defer></script>

</head>

<body>

<header>
    <h1><a href="../../index.html">Carto-Hugo</a> > <a href="../../visualisations.html">Visualisations</a></h1>
    <h2>| Cartes</h2>
</header>

<div class="row" id="interface_container">
        
    <aside class="col" id="toc">

        <div class="sticky">
            <p>Cette maquette de visualisation montre la répartition des occurrences de citations de lieux sur trois cartes différentes.</p>
            <p>La variation des cartes permet un zoom progressif pour pouvoir afficher des données plus précises. Il est possible d'afficher une carte « officielle » au lieu des cartes simplifiées.</p>
            <p>À terme, il pourra être possible de filtrer les données par la table des matières.</p>

            <fieldset>
                <legend>Légende</legend>
                <p>
                    <span style="color: aquamarine;">●</span> : Lieux localisables
                    <br>
                    <span style="color: coral;">●</span> : Lieux incertains ou fictifs
                </p>
            </fieldset>

            <fieldset>
                <legend>Nom des lieux</legend>
                <div>
                    <input type="checkbox" id="place_names" name="place_names" value="place_names" autocomplete="off">
                    <label for="place_names">Afficher le nom des lieux (> 10 occurrences)</label>
                </div>
            </fieldset>

            <div class="tab_container">
                <div class="tab_item">
                    <h3>Table des matières</h3>
                </div>
            </div>

            <ul>
                <li>
                    <span data-indent="0">Livre préliminaire</span>
                </li>
                <li>
                    <span data-indent="0">Œuvre</span>
                    <ul>
                        <li><span data-indent="1">Partie 1</span>
                            <ul>
                                <li><span data-indent="2">Livre 1</span></li>
                                <li><span data-indent="2">Livre 2</span></li>
                                <li><span data-indent="2">Livre 3</span></li>
                                <li><span data-indent="2">Livre 4</span></li>
                                <li><span data-indent="2">Livre 5</span></li>
                                <li><span data-indent="2">Livre 6</span></li>
                                <li><span data-indent="2">Livre 7</span></li>
                            </ul>
                        </li>
                        <li><span data-indent="1">Partie 2</span>
                            <ul>
                                <li><span data-indent="2">Livre 1</span></li>
                                <li><span data-indent="2">Livre 2</span></li>
                                <li><span data-indent="2">Livre 3</span></li>
                                <li><span data-indent="2">Livre 4</span></li>
                            </ul>
                        </li>
                        <li><span data-indent="1">Partie 3</span>
                            <ul>
                                <li><span data-indent="2">Livre 1</span></li>
                                <li><span data-indent="2">Livre 2</span></li>
                                <li><span data-indent="2">Livre 3</span></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>
        
    <main class="col" id="map">
        
        <div id="title">
            Données : Préliminaire + Œuvre
        </div>

        <div class="row">
            <div class="col tab_container">
                <div class="tab_item active" data-target="CG_1794.jpg">
                    <h3>Europe</h3>
                </div>
                <div class="tab_item" data-target="CG_1781.jpg">
                    <h3>Archipel</h3>
                </div>
                <div class="tab_item" data-target="CG_1871.jpg">
                    <h3>Guernesey</h3>
                </div>
            </div>
            <div class="col right" id="toggle_map">
                <h3>⧉</h3>
            </div>
        </div>

        <div class="tab_target active" data-target="CG_1794.jpg">
            <?php echo file_get_contents("assets/svg/europe.svg"); ?>
            <img src="assets/img/CG_1794.jpg">
        </div>
        <div class="tab_target" data-target="CG_1781.jpg">
            <?php echo file_get_contents("assets/svg/archipel.svg"); ?>
            <img src="assets/img/CG_1781.jpg">
        </div>
        <div class="tab_target" data-target="CG_1871.jpg">
            <?php echo file_get_contents("assets/svg/guernesey.svg"); ?>
            <img src="assets/img/CG_1871.jpg">
        </div>
    </main>
</div>

</body>
</html>