# carto-hugo v3

## Prérequis
* git
* libsaxonb-java
* default-jre
* xsltproc

## Installation
* clone repository
* retrieve images and thumbnails (from prod server for the moment)
* launch xsl
See: `data/xslt/README.md`

## Licence
GNU GENERAL PUBLIC LICENSE V3 (voir le [Guide rapide de la GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html))

## Visualisations
La page "visualisations" donne accès à des maquettes de visualisations pour le projet Carto-Hugo, développées par Maxime Bouton et Vincent Maillard  (Protocole astral) 

[Code source avant intégration](https://framagit.org/protocole-astral/carto-hugo-visualisations)

### Histogrammes

Visualisation des occurrences des citations de lieux et de personnages dans l'œuvre.

### Cartes

Visualisation de la répartition des occurrences de citations de lieux sur trois cartes différentes.

### Diagrammes

Visualisation des occurrences des citations de lieux pour chaque chapitre en montrant des extraits de manuscrits ou de cartes.

### Cartes déformées

Visualisation des différences entre les manuscrits et des cartes « officielles ».


